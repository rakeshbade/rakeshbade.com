const AngularApp = angular.module('app', ['ngRoute']);
const scrollMagicCntl = new ScrollMagic.Controller();
AngularApp.config(['$routeProvider','$locationProvider',
    function config($routeProvider,$locationProvider) {
      $routeProvider.
        when('/project/:projectId', {
          templateUrl: 'project.html',
          controller : 'ProjectController'
        }).
        when('/', {
          templateUrl: 'home.html',
          controller : 'MainController'
        }).
        when('/404',{
          template: '<header><h1><center>Page Not Found</center></h1></center><div align="center"><a ng-href="./">HOME</a></div></header>'
        }).
        otherwise('404');
    }
  ]);

AngularApp.controller('MainController', [
  '$scope',
  '$locale',
  'cmnService',
  '$timeout',
  function ($scope, $locale, cmnService, $timeout) {
    $scope._loading = true;
    $scope._page_loader = null;
    $scope._page_data = null;
    $scope._current_project = null;
    cmnService
      .getData()
      .then(function (_page_data) {
        $scope._page_data = _page_data;
      })
      .catch(function (err) {
        $scope.pageLoadErr = err;
      })
      .finally(function () {
        $timeout(function () {
          $scope._loading = false;
          var wow = new WOW({
            boxClass: 'wow', // animated element css class (default is wow)
            animateClass: 'animated', // animation css class (default is animated)
            offset: 10, // distance to the element when triggering the animation (default is 0)
            mobile: false, // trigger animations on mobile devices (default is true)
            live: true, // act on asynchronously loaded content (default is true)
            callback: function (box) {
              // the callback is fired every time an animation is started
              // the argument that is passed in is the DOM node being animated
            },
            scrollContainer: null, // optional scroll container selector, otherwise use window,
            resetAnimation: true, // reset animation on end (default is true)
          });
          wow.init();
        }, 500);
      });

    $scope.sanitizeHTML = cmnService.sanitizeHTML;
  },
]);

/**
Project Controller
**/

AngularApp.controller('ProjectController',['$scope','$routeParams', '$location' , 'cmnService' ,function($scope, $routeParams, $location, cmnService){
  $scope._loading = true;
  if(!$routeParams.projectId) return $location.path('404');
  var _project_id = $routeParams.projectId;
  $scope._loading = true;
  $scope.project = null;
  cmnService
      .getData()
      .then(function (_page_data) {
        if(_page_data && _page_data.portfolio){
          $scope.project = _page_data.portfolio.find(x=>x.index == _project_id);
        }
      })
      .catch(function (err) {
        $scope.pageLoadErr = err;
      }).finally(()=>{
        if(!$scope.project){
          $location.path("/404")
        }
      });
      $scope.sanitizeHTML = cmnService.sanitizeHTML;
}]);

AngularApp.service('cmnService', [
  '$http',
  '$q',
  '$sce',
  '$location',
  function ($http, $q, $sce, $location) {
    this.getData = function () {
      var _deferred = $q.defer();
      let _file_path = './profile.json';
      let project_base = "/project/";
      $http
        .get('./profile.json')
        .then(function (_res) {
          if (_res && _res.data) {
            return _deferred.resolve(_res.data);
          }
          return _deferred.reject('Cannot find the path' + _file_path);
        })
        .catch(function (err) {
          return _deferred.reject(err);
        });
      return _deferred.promise;
    };

    this.sanitizeHTML = function (content) {
      return $sce.trustAsHtml(content);
    };
  },
]);


AngularApp.directive('scrollMagic', [
  'cmnService',
  function (cmnService) {
    return {
      restrict: 'A',
      transclude: false,
      template: '',
      link: function (scope, element, attrs) {},
    };
  },
]);
